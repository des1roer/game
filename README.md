Yii 2 Game
============================

1. Скопировать local.example как local и прописать свои настройки
2. Сделать composer update
3. Применить миграции
    - php yii migrate --migrationPath=@vendor/amnah/yii2-user/migrations
    - php yii migrate
    
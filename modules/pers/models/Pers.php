<?php

namespace app\modules\pers\models;

use Yii;

/**
 * This is the model class for table "pers".
 *
 * @property integer $id
 * @property string $name
 * @property integer $lvl
 * @property integer $money
 * @property string $img
 *
 * @property PersHasParam[] $persHasParams
 * @property Param[] $params
 * @property UserHasPers[] $userHasPers
 * @property User[] $users
 */
class Pers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['lvl', 'money'], 'integer'],
            [['name', 'img'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lvl' => 'Lvl',
            'money' => 'Money',
            'img' => 'Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersHasParams()
    {
        return $this->hasMany(PersHasParam::className(), ['pers_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParams()
    {
        return $this->hasMany(Param::className(), ['id' => 'param_id'])->viaTable('pers_has_param', ['pers_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasPers()
    {
        return $this->hasMany(UserHasPers::className(), ['pers_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_has_pers', ['pers_id' => 'id']);
    }
}

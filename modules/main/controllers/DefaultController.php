<?php

namespace app\modules\main\controllers;

use app\modules\profile\models\Profile;
use app\modules\user\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    /* Для всех */
                    [
                        'allow' => true,
                        'controllers' => ['main/default'],
                        'actions' => ['index', 'error', 'send-chat']
                    ],
                    /* Для гостей */
                    [
//                            'allow' => true,
//                            'controllers' => [''],
//                            'actions' => [''],
//                            'roles' => ['?'],
                    ],
                    /* Для зарегистрированных пользователей */
                    [
//                            'allow' => true,
//                            'controllers' => [''],
//                            'actions' => [''],
//                            'roles' => ['@'],
                    ],
                    /* Для администраторов */
                    [
                        'allow' => true,
                        'controllers' => ['main/default'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],

            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSendChat() {
        if (!empty($_POST)) {
            echo \app\widgets\chat\ChatRoom::sendChat($_POST);
        }
    }

    public function actionIndex()
    {
        $model = User::class;
        return $this->render('index', ['model' => $model]);
    }
}

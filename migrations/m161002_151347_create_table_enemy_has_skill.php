<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%enemy_has_skill}}`.
 */
class m161002_151347_create_table_enemy_has_skill extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%enemy_has_skill}}', [

            'id' => $this->primaryKey()->notNull(),
            'enemy_id' => $this->integer(11)->notNull(),
            'skill_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `enemy_id`
        $this->createIndex(
            'fk_enemy_has_skill_enemy1',
            '{{%enemy_has_skill}}',
            'enemy_id'
        );

        // add foreign key for table `enemy`
        $this->addForeignKey(
            'fk_enemy_has_skill_enemy1',
            '{{%enemy_has_skill}}',
            'enemy_id',
            '{{%enemy}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skill_id`
        $this->createIndex(
            'fk_enemy_has_skill_skill1',
            '{{%enemy_has_skill}}',
            'skill_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `enemy`
        $this->dropForeignKey(
            'fk_enemy_has_skill_enemy1',
            '{{%enemy_has_skill}}'
        );

        // drops index for column `enemy_id`
        $this->dropIndex(
            'fk_enemy_has_skill_enemy1',
            '{{%enemy_has_skill}}'
        );

        // drops index for column `skill_id`
        $this->dropIndex(
            'fk_enemy_has_skill_skill1',
            '{{%enemy_has_skill}}'
        );

        $this->dropTable('{{%enemy_has_skill}}');
    }
}

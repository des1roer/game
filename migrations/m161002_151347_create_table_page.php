<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%page}}`.
 */
class m161002_151347_create_table_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page}}', [

            'id' => $this->primaryKey()->notNull(),
            'txt' => $this->string(45),
            'link' => $this->string(45),
            'img' => $this->string(45),
            'type_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `type_id`
        $this->createIndex(
            'fk_page_type1',
            '{{%page}}',
            'type_id'
        );

        // add foreign key for table `type`
        $this->addForeignKey(
            'fk_page_type1',
            '{{%page}}',
            'type_id',
            '{{%type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `type`
        $this->dropForeignKey(
            'fk_page_type1',
            '{{%page}}'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            'fk_page_type1',
            '{{%page}}'
        );

        $this->dropTable('{{%page}}');
    }
}

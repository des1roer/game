<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%castle}}`.
 */
class m161002_151347_create_table_castle extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%castle}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45)->notNull(),
            'lvl' => $this->integer(11),
            'img' => $this->string(255),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%castle}}');
    }
}

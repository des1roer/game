<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%enemy}}`.
 */
class m161002_151347_create_table_enemy extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%enemy}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'lvl' => $this->integer(11),
            'exp' => $this->integer(11),
            'cost' => $this->integer(11),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%enemy}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%page_has_btn}}`.
 */
class m161002_151347_create_table_page_has_btn extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%page_has_btn}}', [

            'id' => $this->primaryKey()->notNull(),
            'page_id' => $this->integer(11)->notNull(),
            'btn_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `page_id`
        $this->createIndex(
            'fk_page_has_btn_page1',
            '{{%page_has_btn}}',
            'page_id'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'fk_page_has_btn_page1',
            '{{%page_has_btn}}',
            'page_id',
            '{{%page}}',
            'id',
            'CASCADE'
        );

        // creates index for column `btn_id`
        $this->createIndex(
            'fk_page_has_btn_btn1',
            '{{%page_has_btn}}',
            'btn_id'
        );

        // add foreign key for table `btn`
        $this->addForeignKey(
            'fk_page_has_btn_btn1',
            '{{%page_has_btn}}',
            'btn_id',
            '{{%btn}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `page`
        $this->dropForeignKey(
            'fk_page_has_btn_page1',
            '{{%page_has_btn}}'
        );

        // drops index for column `page_id`
        $this->dropIndex(
            'fk_page_has_btn_page1',
            '{{%page_has_btn}}'
        );

        // drops foreign key for table `btn`
        $this->dropForeignKey(
            'fk_page_has_btn_btn1',
            '{{%page_has_btn}}'
        );

        // drops index for column `btn_id`
        $this->dropIndex(
            'fk_page_has_btn_btn1',
            '{{%page_has_btn}}'
        );

        $this->dropTable('{{%page_has_btn}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%enemy_has_param}}`.
 */
class m161002_151347_create_table_enemy_has_param extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%enemy_has_param}}', [

            'id' => $this->primaryKey()->notNull(),
            'enemy_id' => $this->integer(11)->notNull(),
            'param_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `enemy_id`
        $this->createIndex(
            'fk_enemy_has_param_enemy1',
            '{{%enemy_has_param}}',
            'enemy_id'
        );

        // add foreign key for table `enemy`
        $this->addForeignKey(
            'fk_enemy_has_param_enemy1',
            '{{%enemy_has_param}}',
            'enemy_id',
            '{{%enemy}}',
            'id',
            'CASCADE'
        );

        // creates index for column `param_id`
        $this->createIndex(
            'fk_enemy_has_param_param1',
            '{{%enemy_has_param}}',
            'param_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `enemy`
        $this->dropForeignKey(
            'fk_enemy_has_param_enemy1',
            '{{%enemy_has_param}}'
        );

        // drops index for column `enemy_id`
        $this->dropIndex(
            'fk_enemy_has_param_enemy1',
            '{{%enemy_has_param}}'
        );

        // drops index for column `param_id`
        $this->dropIndex(
            'fk_enemy_has_param_param1',
            '{{%enemy_has_param}}'
        );

        $this->dropTable('{{%enemy_has_param}}');
    }
}

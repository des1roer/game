<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%enemy_has_item}}`.
 */
class m161002_151347_create_table_enemy_has_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%enemy_has_item}}', [

            'id' => $this->primaryKey()->notNull(),
            'enemy_id' => $this->integer(11)->notNull(),
            'item_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `enemy_id`
        $this->createIndex(
            'fk_enemy_has_item_enemy1',
            '{{%enemy_has_item}}',
            'enemy_id'
        );

        // add foreign key for table `enemy`
        $this->addForeignKey(
            'fk_enemy_has_item_enemy1',
            '{{%enemy_has_item}}',
            'enemy_id',
            '{{%enemy}}',
            'id',
            'CASCADE'
        );

        // creates index for column `item_id`
        $this->createIndex(
            'fk_enemy_has_item_item1',
            '{{%enemy_has_item}}',
            'item_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `enemy`
        $this->dropForeignKey(
            'fk_enemy_has_item_enemy1',
            '{{%enemy_has_item}}'
        );

        // drops index for column `enemy_id`
        $this->dropIndex(
            'fk_enemy_has_item_enemy1',
            '{{%enemy_has_item}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            'fk_enemy_has_item_item1',
            '{{%enemy_has_item}}'
        );

        $this->dropTable('{{%enemy_has_item}}');
    }
}

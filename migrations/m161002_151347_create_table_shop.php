<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%shop}}`.
 */
class m161002_151347_create_table_shop extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%shop}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'img' => $this->string(255),
            'type_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `type_id`
        $this->createIndex(
            'fk_shop_type1',
            '{{%shop}}',
            'type_id'
        );

        // add foreign key for table `type`
        $this->addForeignKey(
            'fk_shop_type1',
            '{{%shop}}',
            'type_id',
            '{{%type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `type`
        $this->dropForeignKey(
            'fk_shop_type1',
            '{{%shop}}'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            'fk_shop_type1',
            '{{%shop}}'
        );

        $this->dropTable('{{%shop}}');
    }
}

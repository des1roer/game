<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%story}}`.
 */
class m161002_151347_create_table_story extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%story}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'parent_id' => $this->string(45),
            'story_id' => $this->integer(11)->notNull(),
            'page_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `story_id`
        $this->createIndex(
            'fk_story_story1',
            '{{%story}}',
            'story_id'
        );

        // add foreign key for table `story`
        $this->addForeignKey(
            'fk_story_story1',
            '{{%story}}',
            'story_id',
            '{{%story}}',
            'id',
            'CASCADE'
        );

        // creates index for column `page_id`
        $this->createIndex(
            'fk_story_page1',
            '{{%story}}',
            'page_id'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'fk_story_page1',
            '{{%story}}',
            'page_id',
            '{{%page}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `story`
        $this->dropForeignKey(
            'fk_story_story1',
            '{{%story}}'
        );

        // drops index for column `story_id`
        $this->dropIndex(
            'fk_story_story1',
            '{{%story}}'
        );

        // drops foreign key for table `page`
        $this->dropForeignKey(
            'fk_story_page1',
            '{{%story}}'
        );

        // drops index for column `page_id`
        $this->dropIndex(
            'fk_story_page1',
            '{{%story}}'
        );

        $this->dropTable('{{%story}}');
    }
}

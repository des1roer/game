<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%building}}`.
 */
class m161002_151347_create_table_building extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%building}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'lvl' => $this->string(45),
            'cost' => $this->string(45),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%building}}');
    }
}

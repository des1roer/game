<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%btn}}`.
 */
class m161002_151347_create_table_btn extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%btn}}', [

            'id' => $this->primaryKey()->notNull(),
            'link' => $this->string(45),
            'sort' => $this->string(45),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%btn}}');
    }
}

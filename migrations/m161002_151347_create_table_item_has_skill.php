<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%item_has_skill}}`.
 */
class m161002_151347_create_table_item_has_skill extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%item_has_skill}}', [

            'id' => $this->primaryKey()->notNull(),
            'item_id' => $this->integer(11)->notNull(),
            'skill_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `item_id`
        $this->createIndex(
            'fk_item_has_skill_item1',
            '{{%item_has_skill}}',
            'item_id'
        );

        // add foreign key for table `item`
        $this->addForeignKey(
            'fk_item_has_skill_item1',
            '{{%item_has_skill}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skill_id`
        $this->createIndex(
            'fk_item_has_skill_skill1',
            '{{%item_has_skill}}',
            'skill_id'
        );

        // add foreign key for table `skill`
        $this->addForeignKey(
            'fk_item_has_skill_skill1',
            '{{%item_has_skill}}',
            'skill_id',
            '{{%skill}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `item`
        $this->dropForeignKey(
            'fk_item_has_skill_item1',
            '{{%item_has_skill}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            'fk_item_has_skill_item1',
            '{{%item_has_skill}}'
        );

        // drops foreign key for table `skill`
        $this->dropForeignKey(
            'fk_item_has_skill_skill1',
            '{{%item_has_skill}}'
        );

        // drops index for column `skill_id`
        $this->dropIndex(
            'fk_item_has_skill_skill1',
            '{{%item_has_skill}}'
        );

        $this->dropTable('{{%item_has_skill}}');
    }
}

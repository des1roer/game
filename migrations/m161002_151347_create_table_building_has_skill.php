<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%building_has_skill}}`.
 */
class m161002_151347_create_table_building_has_skill extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%building_has_skill}}', [

            'id' => $this->primaryKey()->notNull(),
            'building_id' => $this->integer(11)->notNull(),
            'skill_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `building_id`
        $this->createIndex(
            'fk_building_has_skill_building1',
            '{{%building_has_skill}}',
            'building_id'
        );

        // add foreign key for table `building`
        $this->addForeignKey(
            'fk_building_has_skill_building1',
            '{{%building_has_skill}}',
            'building_id',
            '{{%building}}',
            'id',
            'CASCADE'
        );

        // creates index for column `skill_id`
        $this->createIndex(
            'fk_building_has_skill_skill1',
            '{{%building_has_skill}}',
            'skill_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `building`
        $this->dropForeignKey(
            'fk_building_has_skill_building1',
            '{{%building_has_skill}}'
        );

        // drops index for column `building_id`
        $this->dropIndex(
            'fk_building_has_skill_building1',
            '{{%building_has_skill}}'
        );

        // drops index for column `skill_id`
        $this->dropIndex(
            'fk_building_has_skill_skill1',
            '{{%building_has_skill}}'
        );

        $this->dropTable('{{%building_has_skill}}');
    }
}

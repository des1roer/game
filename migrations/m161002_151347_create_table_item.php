<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%item}}`.
 */
class m161002_151347_create_table_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%item}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45),
            'img' => $this->string(255),
            'lvl' => $this->integer(11),
            'cost' => $this->integer(11),
            'rune_cnt' => $this->integer(11),
            'type_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `type_id`
        $this->createIndex(
            'fk_item_type1',
            '{{%item}}',
            'type_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops index for column `type_id`
        $this->dropIndex(
            'fk_item_type1',
            '{{%item}}'
        );

        $this->dropTable('{{%item}}');
    }
}

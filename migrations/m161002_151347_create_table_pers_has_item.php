<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers_has_item}}`.
 */
class m161002_151347_create_table_pers_has_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers_has_item}}', [

            'id' => $this->primaryKey()->notNull(),
            'pers_id' => $this->integer(11)->notNull(),
            'item_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `pers_id`
        $this->createIndex(
            'fk_pers_has_item_pers1',
            '{{%pers_has_item}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'fk_pers_has_item_pers1',
            '{{%pers_has_item}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE'
        );

        // creates index for column `item_id`
        $this->createIndex(
            'fk_pers_has_item_item1',
            '{{%pers_has_item}}',
            'item_id'
        );

        // add foreign key for table `item`
        $this->addForeignKey(
            'fk_pers_has_item_item1',
            '{{%pers_has_item}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'fk_pers_has_item_pers1',
            '{{%pers_has_item}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'fk_pers_has_item_pers1',
            '{{%pers_has_item}}'
        );

        // drops foreign key for table `item`
        $this->dropForeignKey(
            'fk_pers_has_item_item1',
            '{{%pers_has_item}}'
        );

        // drops index for column `item_id`
        $this->dropIndex(
            'fk_pers_has_item_item1',
            '{{%pers_has_item}}'
        );

        $this->dropTable('{{%pers_has_item}}');
    }
}

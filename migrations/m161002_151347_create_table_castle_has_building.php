<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%castle_has_building}}`.
 */
class m161002_151347_create_table_castle_has_building extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%castle_has_building}}', [

            'id' => $this->primaryKey()->notNull(),
            'castle_id' => $this->integer(11)->notNull(),
            'building_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `castle_id`
        $this->createIndex(
            'fk_castle_has_building_castle1',
            '{{%castle_has_building}}',
            'castle_id'
        );

        // add foreign key for table `castle`
        $this->addForeignKey(
            'fk_castle_has_building_castle1',
            '{{%castle_has_building}}',
            'castle_id',
            '{{%castle}}',
            'id',
            'CASCADE'
        );

        // creates index for column `building_id`
        $this->createIndex(
            'fk_castle_has_building_building1',
            '{{%castle_has_building}}',
            'building_id'
        );

        // add foreign key for table `building`
        $this->addForeignKey(
            'fk_castle_has_building_building1',
            '{{%castle_has_building}}',
            'building_id',
            '{{%building}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `castle`
        $this->dropForeignKey(
            'fk_castle_has_building_castle1',
            '{{%castle_has_building}}'
        );

        // drops index for column `castle_id`
        $this->dropIndex(
            'fk_castle_has_building_castle1',
            '{{%castle_has_building}}'
        );

        // drops foreign key for table `building`
        $this->dropForeignKey(
            'fk_castle_has_building_building1',
            '{{%castle_has_building}}'
        );

        // drops index for column `building_id`
        $this->dropIndex(
            'fk_castle_has_building_building1',
            '{{%castle_has_building}}'
        );

        $this->dropTable('{{%castle_has_building}}');
    }
}

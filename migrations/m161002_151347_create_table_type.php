<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%type}}`.
 */
class m161002_151347_create_table_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%type}}', [

            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string(45)->notNull(),
            'type_id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `type_id`
        $this->createIndex(
            'fk_type_type1',
            '{{%type}}',
            'type_id'
        );

        // add foreign key for table `type`
        $this->addForeignKey(
            'fk_type_type1',
            '{{%type}}',
            'type_id',
            '{{%type}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `type`
        $this->dropForeignKey(
            'fk_type_type1',
            '{{%type}}'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            'fk_type_type1',
            '{{%type}}'
        );

        $this->dropTable('{{%type}}');
    }
}
